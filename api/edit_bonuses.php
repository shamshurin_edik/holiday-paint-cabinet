<?php
if (!empty($_POST['data'])) {
  $item = json_decode( $_POST['data'] );
  $id = $item->{'id'};
  $val = $item->{'val'};

  require_once 'config.php';
  $table = 'users_info';

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    $mysqli->set_charset("utf8");
    $edit_item = $mysqli->prepare("UPDATE $table SET bonuses = bonuses + ? WHERE id=?");
    $edit_item->bind_param("ii", $val, $id);
    $edit = $edit_item->execute();
    $edit_item->close();
    if ($edit) $data['error'] = 0;
    $mysqli->close();
  }
} else $data['error'] = 1;

echo json_encode($data);
?>
