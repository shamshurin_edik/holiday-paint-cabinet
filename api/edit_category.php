<?php
if (!empty($_POST['data'])) {
  $category = json_decode( $_POST['data'] );
  $id = $category->{'id'};
  $action = $category->{'action'};

  require_once 'config.php';
  $table = 'categories';

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    $mysqli->set_charset("utf8");

    if ($action == 'visible') {
      $edit_category = $mysqli->prepare("UPDATE $table SET visible = IF ( visible=1, 0, 1 ) WHERE id=?");
      $edit_category->bind_param("i", $id);
      $edit = $edit_category->execute();
      $edit_category->close();
      if ($edit) $data['error'] = 0;
    }

    if ($action == 'remove') {
      $edit_category = $mysqli->prepare("DELETE FROM $table WHERE id = ?");
      $edit_category->bind_param("i", $id);
      $edit = $edit_category->execute();
      $edit_category->close();
      if ($edit) $data['error'] = 0;
    }

    $mysqli->close();
  }
} else $data['error'] = 1;

echo json_encode($data);
?>
