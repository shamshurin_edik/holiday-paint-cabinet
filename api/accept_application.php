<?php
if (!empty($_POST['data'])) {

  $application = json_decode($_POST['data']);
  $id_application = $application->{'id'};
  $table = 'applications';
  require_once 'config.php'; // подключаем скрипт
  $link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD)
    or die('Не удалось соединиться: ' . mysqli_error($link));
  mysqli_select_db($link,DB_NAME) or die('Не удалось выбрать базу данных');
  mysqli_set_charset($link, 'utf8');

  $query = "SELECT * FROM $table WHERE `id`=$id_application";
  $result = mysqli_query($link, $query) or die('Запрос не удался: ' . mysqli_error($link));
  if ($result) {
    $application_data = mysqli_fetch_array($result);
    $user_id = $application_data['user_id'];

    if (isset($application->{'location'})) {
      $location = $application->{'location'};
      if ($location == 'order') {
        $items = json_decode($application_data['items'])->{'items'};
      }
      if ($location == 'stock') {
        $items = json_decode($application_data['items']);
      }
    } else {
      $location = false;
    }




    $value = '';
    for ($i=0; $i < count($items); $i++) {
      $id = $items[$i]->{'id'};
      $quantity = $items[$i]->{'quantity'};
      $value .="($id, $quantity, $user_id)";
      if (($i + 1) < count($items)) $value .=", ";
    }
    //echo $value;
    $table_two  = 'stock';

    if ($location == 'order') {
      $query = "INSERT INTO $table_two (goods_id, quantity, user_id) VALUES $value ON DUPLICATE KEY UPDATE quantity=quantity-VALUES(quantity)";
    }
    if ($location == 'stock') {
      $query = "INSERT INTO $table_two (goods_id, quantity, user_id) VALUES $value ON DUPLICATE KEY UPDATE quantity=quantity+VALUES(quantity)";
    }

    $result = mysqli_query($link, $query) or die('Запрос не удался: ' . mysqli_error($link));
    if ($result) {
      $query = "UPDATE $table SET `status`=1 WHERE `id`=$id_application";
      $result = mysqli_query($link, $query) or die('Запрос не удался: ' . mysqli_error($link));


      $data['error'] = 0;
    } else $data['error'] = 2;
  } else {
    $data['error'] = 1;
  }
  mysqli_close($link);
} else $data['error'] = 3;
echo json_encode($data);
?>
