<?php
if (!empty($_POST['data'])) {

  require_once 'config.php'; // подключаем скрипт
  $table = 'promocodes';
  $promo = json_decode( $_POST['data'] );

  if (isset($promo->{'id'})) {
    $act = 'edit';
    $id = $promo->{'id'};
  } else {
    $act = 'new';
  }

  $name = $promo->{'name'};
  $code = $promo->{'code'};

  $detail = new \stdClass();
  $detail->{'groupe'} = $promo->{'groupe'};
  $detail->{'type'} = $promo->{'type'};
  $detail->{'sum'} = $promo->{'sum'};
  $detail->{'good'} = $promo->{'good'};
  $detail->{'category'} = $promo->{'category'};
  $detail->{'limit'} = $promo->{'limit'};
  $detail->{'finish'} = $promo->{'finish'};

  $details = json_encode($detail);

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    //echo $items;
    $mysqli->set_charset("utf8");

    if ($act == 'new') {
      $edit_promo = $mysqli->prepare("INSERT INTO $table (name, code, detail) VALUES (?, ?, ?)");
      $edit_promo->bind_param("sss", $name, $code, $details);
    }

    if ($act == 'edit') {
      $edit_promo = $mysqli->prepare("UPDATE $table SET name=?, detail=? WHERE id=?");
      $edit_promo->bind_param("ssi", $name, $details, $id);
    }

    $edit = $edit_promo->execute();
    if ($edit) $data['error'] = 0;
    if ($act == 'new') $data['id'] = $mysqli->insert_id;
    $edit_promo->close();
    $mysqli->close();

  }

} else $data['error'] = 1;

echo json_encode($data);
?>
