<?php
if (!empty($_POST['data'])) {
  $user = json_decode( $_POST['user'] );
  $items = $_POST['data'];
  $user_id = $user->{'user_id'};
  //$goods_id = $goods->{'goods_id'};
  //$quantity = $goods->{'quantity'};
  require_once 'config.php'; // подключаем скрипт
  $table = 'applications';
  $table_two = 'partner_cart';
  $location = 'stock';

  //$items = json_encode($goods);

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    //echo $items;
    $mysqli->set_charset("utf8");
    $save_application = $mysqli->prepare("INSERT INTO $table (user_id, items, action) VALUES (?, ?, ?)");
    $save_application->bind_param("iss", $user_id, $items, $location);
    $insert = $save_application->execute();
    $save_application->close();
    if ($insert) {
      $n_application = $mysqli->insert_id;
      $clear_cart = $mysqli->prepare("DELETE FROM $table_two WHERE user_id = ?");
      $clear_cart->bind_param("i", $user_id);
      $clear = $clear_cart->execute();
      $clear_cart->close();
      if ($clear) {
        $data['error'] = 0;



        $goods_table = 'goods';
        $get_goods = $mysqli->prepare("SELECT name, price_partner  FROM $goods_table WHERE id = ?");
        $get_goods->bind_param("i", $id_goods);

        $order_items = array();

        $table = '';
        $all_price = 0;
        $items_ar = json_decode($items);
        foreach ($items_ar as $key => $value) {
          $id_goods = $value->{'id'};
          $get_goods->execute();
          $result = $get_goods->get_result();
          $goors_row = $result->fetch_array(MYSQLI_ASSOC);

          $itms = new \stdClass();
          $itms->{'name'} = $goors_row['name'];
          $itms->{'price'} = $goors_row['price_partner'];
          $itms->{'quantity'} = $value->{'quantity'};
          $itms->{'sum'} = $value->{'quantity'} * $goors_row['price_partner'];
          $all_price = $all_price + $itms->{'sum'};
          $order_items[] = $itms;


          $table .= '<tr>
            <th style="font-weight: 300; text-align: left;">' . $itms->{"name"} . '</th>
            <th style="font-weight: 300; text-align: left; width:80px;">' . $itms->{"price"} . 'р.</th>
            <th style="font-weight: 300; width:80px;">' . $itms->{"quantity"} . '</th>
            <th style="font-weight: 300; text-align: right; width:80px;">' . $itms->{"sum"} . 'р.</th>
          </tr>';

        }

        $table_one = 'users_info';
        $table_two = 'cities';
        $tabele_three = 'users';
        $stmt = $mysqli->prepare("SELECT $table_one.*, $table_two.name AS city, $tabele_three.email FROM $table_one INNER JOIN $tabele_three ON $tabele_three.id = ? INNER JOIN $table_two ON $table_one.city = $table_two.id AND $table_one.id = ?");
        $stmt->bind_param("ii", $user_id, $user_id);
        $stmt->execute();
        $result = $stmt->get_result();
        $partner = $result->fetch_assoc();
        $email = $partner['email'];
        $city = $partner['city'];
        $name = $partner['name'];
        $phone = $partner['phone'];
        $to = 'zakaz@holiday-paint.ru'; // обратите внимание на запятую
        //$client = $email;
        //$to = 'shamshurin.edik@gmail.com'; // обратите внимание на запятую




        // тема письма
        $subject = 'Заказ ' . $city . ' от ' . $name . '(№' . $n_application . ')';
        $subject_partner = 'Holiday-Paint - Ваш заказ';



        $message = '<html>
        <head>
          <title>' . $subject . '</title>
        </head>
        <body style=" font-family: sans-serif;">
          <table border="0" style="max-width: 100%; width: 600px; margin: 0 auto 45px;">
            <tr>
              <th style="color: #00b8f6; vertical-align: top; font-size: 38px; text-transform: uppercase; font-weight: 800; padding: 0; text-align: left;">
                <p style="line-height: 1; margin: 0; padding: 0;">Holiday<br> Paint</p>
              </th>
              <th style="width: 200px; vertical-align: top; text-align: right;">
                <img src="http://my.holiday-paint.ru/logo.png" alt="" style="max-width: 150px; height: auto;">
              </th>
            </tr>
          </table>

          <table border="0" style="max-width: 100%; width: 600px; text-align: left; margin: 0 auto 15px;">
            <tr>
              <th>
                <p style="color: #00b8f6; vertical-align: top; font-size: 24px; text-transform: uppercase; margin: 0;">г. ' . $city . '</p>
              </th>
              <th style="width: 100px; vertical-align: top; font-weight: 300; color: #999;">Дата заказа</th>
              <th style="width: 70px; vertical-align: top; color: #00b8f6; vertical-align: top; font-size: 16px;">' . date("d/m/y") . '</th>
            </tr>
            <tr>
              <th>
              </th>
              <th style="width: 100px; vertical-align: top; font-weight: 300; color: #999;">Номер заказа</th>
              <th style="width: 70px; vertical-align: top; color: #00b8f6; vertical-align: top; font-size: 16px;">' . $n_application . '</th>
            </tr>
          </table>


          <div style="background-color: #00b8f6; text-align: center; padding: 15px; margin: 0 auto; max-width: 620px;">

            <table border="0" style="padding: 10px 0; border-bottom: 2px solid rgb(114, 211, 240); font-size: 14px; max-width: 100%; width: 600px; margin: 0 auto; color: #FFF; text-transform: uppercase;">
              <tr>
                <th style="font-weight: 300; text-align: left;">Наименование</th>
                <th style="font-weight: 300; text-align: left; width:80px;">Цена</th>
                <th style="font-weight: 300; width:80px;">Кол-во</th>
                <th style="font-weight: 300; text-align: right; width:80px;">Сумма</th>
              </tr>
            </table>

            <table border="0" style="padding: 10px 0; border-bottom: 2px solid rgb(114, 211, 240); font-size: 14px; max-width: 100%; width: 600px; margin: 0 auto; color: #FFF; text-transform: uppercase;">
              ' . $table . '
            </table>


            <table border="0" style="padding: 20px 0 10px;  font-size: 14px; max-width: 100%; width: 600px; margin: 0 auto; color: #FFF; text-transform: uppercase;">
              <tr>
                <th style="text-align: left;">

                </th>

                <th style="font-weight: 300; text-align: right; width:180px;">
                  Итого <span style="font-size: 24px; font-weight: 700;">' . $all_price . ' р.</span>
                </th>
              </tr>

            </table>



          </div>

          <table border="0" style="padding: 20px 0 10px;  font-size: 14px; max-width: 100%; width: 600px; margin: 0 auto; color: #999; text-transform: none;">
            <tr>
              <th style="text-align: left; font-weight: 300;">
                Город: ' . $city . '<br>
                Имя: ' . $name . '<br>
                Телефон: +7' . $phone . '<br>
                E-Mail: ' . $email . '
              </th>

            </tr>

          </table>

          <div style="background-color: #00b8f6; text-align: center; padding: 15px;">
        </div>

        <a href="http://my.holiday-paint.ru/report" style="text-transform: uppercase; padding: 10px 20px; text-decoration: none;  margin: 45px auto; display: block; text-align: center;">
          <img alt="HoliCloude" src="http://my.holiday-paint.ru/btn.jpg" style="max-width: 300px; height: auto;"/>
        </a>

        </body>
        </html>
        ';




        $message_partner = '<html>
        <head>
          <title>' . $subject_partner . '</title>
        </head>
        <body style=" font-family: sans-serif;">
          <table border="0" style="max-width: 100%; width: 600px; margin: 0 auto 45px;">
            <tr>
              <th style="color: #00b8f6; vertical-align: top; font-size: 38px; text-transform: uppercase; font-weight: 800; padding: 0; text-align: left;">
                <p style="line-height: 1; margin: 0; padding: 0;">Holiday<br> Paint</p>
              </th>
              <th style="width: 200px; vertical-align: top; text-align: right;">
                <img src="http://my.holiday-paint.ru/logo.png" alt="" style="max-width: 150px; height: auto;">
              </th>
            </tr>
          </table>

          <table border="0" style="max-width: 100%; width: 600px; text-align: left; margin: 0 auto 15px;">
            <tr>
              <th>
                <p style="color: #00b8f6; vertical-align: top; font-size: 24px; text-transform: uppercase; margin: 0;">Ваш заказ</p>
              </th>
              <th style="width: 100px; vertical-align: top; font-weight: 300; color: #999;">Дата заказа</th>
              <th style="width: 70px; vertical-align: top; color: #00b8f6; vertical-align: top; font-size: 16px;">' . date("d/m/y") . '</th>
            </tr>
            <tr>
              <th>
              </th>
              <th style="width: 100px; vertical-align: top; font-weight: 300; color: #999;">Номер заказа</th>
              <th style="width: 70px; vertical-align: top; color: #00b8f6; vertical-align: top; font-size: 16px;">' . $n_application . '</th>
            </tr>
          </table>


          <div style="background-color: #00b8f6; text-align: center; padding: 15px; margin: 0 auto; max-width: 620px;">

            <table border="0" style="padding: 10px 0; border-bottom: 2px solid rgb(114, 211, 240); font-size: 14px; max-width: 100%; width: 600px; margin: 0 auto; color: #FFF; text-transform: uppercase;">
              <tr>
                <th style="font-weight: 300; text-align: left;">Наименование</th>
                <th style="font-weight: 300; text-align: left; width:80px;">Цена</th>
                <th style="font-weight: 300; width:80px;">Кол-во</th>
                <th style="font-weight: 300; text-align: right; width:80px;">Сумма</th>
              </tr>
            </table>

            <table border="0" style="padding: 10px 0; border-bottom: 2px solid rgb(114, 211, 240); font-size: 14px; max-width: 100%; width: 600px; margin: 0 auto; color: #FFF; text-transform: uppercase;">
              ' . $table . '
            </table>


            <table border="0" style="padding: 20px 0 10px;  font-size: 14px; max-width: 100%; width: 600px; margin: 0 auto; color: #FFF; text-transform: uppercase;">
              <tr>
                <th style="text-align: left;">
                </th>

                <th style="font-weight: 300; text-align: right; width:180px;">
                  Итого <span style="font-size: 24px; font-weight: 700;">' . $all_price . ' р.</span>
                </th>
              </tr>

            </table>



          </div>

          <table border="0" style="padding: 20px 0 10px;  font-size: 14px; max-width: 100%; width: 600px; margin: 0 auto; color: #999; text-transform: none;">
            <tr>
              <th style="text-align: left; font-weight: 300;">
                Город: ' . $city . '<br>
                Имя: ' . $name . '<br>
                Телефон: ' . $phone . '<br>
                E-Mail: ' . $email . '
              </th>

            </tr>

          </table>

          <div style="background-color: #00b8f6; text-align: center; padding: 15px;">
        </div>

        <a href="http://my.holiday-paint.ru/history" style="text-transform: uppercase; padding: 10px 20px; text-decoration: none;  margin: 45px auto; display: block; text-align: center;">
          <img alt="HoliCloude" src="http://my.holiday-paint.ru/btn.jpg" style="max-width: 300px; height: auto;"/>
        </a>
        </body>
        </html>
        ';


        $headers  = 'MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'From: HOLIDAY PAINT <zakaz@holiday-paint.ru>';

        // Отправляем
        mail($to, $subject, $message, $headers);
        mail($email, $subject_partner, $message_partner, $headers);



      } else $data['error'] = 4;
    } else $data['error'] = 3;

    $mysqli->close();

  }

} else $data['error'] = 1;

echo json_encode($data);

?>
