<?php
if (!empty($_POST['data'])) {

  $application = json_decode($_POST['data']);
  $id_application = $application->{'id'};

  $table = 'applications';
  $table_two = 'goods';

  $data = array();


  require_once 'config.php'; // подключаем скрипт

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    $mysqli->set_charset("utf8");

    $stmt = $mysqli->prepare("SELECT * FROM $table WHERE `id`=?");
    $stmt->bind_param("i", $id_application);
    $stmt->execute();

    $result = $stmt->get_result();

    $application_data = $result->fetch_array(MYSQLI_ASSOC);
    if (isset($application->{'location'})) {
      $location = $application->{'location'};
      if ($location == 'order' || $location == 'offline') {
        $items = json_decode($application_data['items'])->{'items'};
        $buyer = json_decode($application_data['items'])->{'buyer'};
        if (isset(json_decode($application_data['items'])->{'discount'})) {
          $discount = json_decode($application_data['items'])->{'discount'};
          $data['discount'] = $discount;
        }
        $data['buyer'] = $buyer;
      }
      if ($location == 'stock') {
        //print_r($application_data);
        $items = json_decode($application_data['items']);
      }
    } else {
      $items = json_decode($application_data['items']);
    }
    $app = $mysqli->prepare("SELECT * FROM $table_two WHERE `id`=?");
    $app->bind_param("i", $id);
    for ($i=0; $i < count($items); $i++) {
      $id = $items[$i]->{'id'};
      $quantity = $items[$i]->{'quantity'};
      $app->execute();
      $app_result = $app->get_result();
      $item = $app_result->fetch_array(MYSQLI_ASSOC);
      $itm = new \stdClass();
      $itm->{'name'} = $item['name'];
      $itm->{'quantity'} = $quantity;
      if ($location == 'offline') $itm->{'price'} = $items[$i]->{'price'};
      $data['items'][] = $itm;
    }
    $stmt->close();
    $mysqli->close();
  }
} else  $data['error'] = 1;

echo json_encode($data);

?>
