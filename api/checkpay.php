<?php

function buildResponse($functionName, $invoiceId, $shopId, $result_code, $message = null) {
  $date = new DateTime();

    $performedDatetime = $date->format("Y-m-d") . "T" . $date->format("H:i:s") . ".000" . $date->format("P");
    $response = '<?xml version="1.0" encoding="UTF-8"?><' . $functionName . 'Response performedDatetime="' . $performedDatetime .
        '" code="' . $result_code . '" ' . ($message != null ? 'message="' . $message . '"' : "") . ' invoiceId="' . $invoiceId . '" shopId="' . $shopId . '"/>';
    return $response;

}

$functionName = $_POST['action'];
$result_code = 0;
$invoiceId = $_POST['invoiceId'];
$shopId = $_POST['shopId'];

echo (buildResponse($functionName, $invoiceId, $shopId, $result_code));

?>
