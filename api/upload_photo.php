<?php
$uploaddir = 'media/photos/';
$pieces = explode("/", $_FILES['image']['type']);
$extension = $pieces[1]; // кусок2
$filename = uniqid() . '.' . $extension;
$uploadfile = $uploaddir . $filename;
$_FILES['image']['type'];


$info = json_decode( $_POST['info'] );
$array = ($info->{'hashtags'});
$user_id = ($info->{'user_id'});
$level = ($info->{'level'});
$hashtags = implode(":", $array);

function resize($file_input, $file_output, $w_o, $h_o) {
  list($w_i, $h_i, $type) = getimagesize($file_input);
  if (!$w_i || !$h_i) {
    echo 'Невозможно получить длину и ширину изображения при уменьшении';
    return;
  }
  $types = array('','gif','jpeg','png');
  $ext = $types[$type];
  if ($ext) {
    $func = 'imagecreatefrom'.$ext;
    $img = $func($file_input);
  } else {
    echo 'Некорректный формат файла';
    return;
  }
  if (!$h_o) $h_o = $w_o/($w_i/$h_i);
  if (!$w_o) $w_o = $h_o/($h_i/$w_i);
  $img_o = imagecreatetruecolor($w_o, $h_o);
  imageAlphaBlending($img_o, false); //не смешиваем, а заменяем
  imageSaveAlpha($img_o,true);
  imagecopyresampled($img_o, $img, 0, 0, 0, 0, $w_o, $h_o, $w_i, $h_i);
  if ($type == 2) {
    return imagejpeg($img_o,$file_output,100);
  } else {
    $func = 'image'.$ext;
    return $func($img_o,$file_output);
  }
 }

if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
  $data['name'] = $filename;

  $nw = 250;    // Ширина миниатюр
  $nh = 100;    // Высота миниатюр
  $dest = $uploaddir . "th_" . $filename;   // Файл с результатом работы
  resize($uploadfile, $dest, $nw,'');

  require_once 'config.php'; // подключаем скрипт
  $table = 'photos';

  if ($level == 2) {
    $status = 1;
  } else {
    $status = 0;
  }


  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    $mysqli->set_charset("utf8");

    $stmt = $mysqli->prepare("INSERT INTO $table (user_id, name, hashtags, status) VALUES (?, ?, ?, ?)");
    $stmt->bind_param("issi", $user_id, $filename, $hashtags, $status);
    $stmt->execute();
    $data['id'] = $mysqli->insert_id;
    $data['status'] = $status;
    $stmt->close();
    $mysqli->close();
    $data['error'] = 0;
  }

} else $data['error'] = 1;

echo json_encode($data);
?>
