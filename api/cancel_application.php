<?php
if (!empty($_POST['data'])) {

  $application = json_decode($_POST['data']);
  $id_application = $application->{'id'};
  if (isset($application->{'location'})) {
    $location = $application->{'location'};
  } else $location = false;

  $table = 'applications';
  $table_two = 'stock';
  require_once 'config.php'; // подключаем скрипт

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    $mysqli->set_charset("utf8");




    if (isset($application->{'status'})) {
      if ($application->{'status'} == 2) $status = 2;
      if ($application->{'status'} == 0) $status = 2;
      if ($application->{'status'} == 1) {
        $status = 0;
        if ($location == 'stock' || $location == 'order' || $location == 'offline') {
          $get_items = $mysqli->prepare("SELECT * FROM $table WHERE `id`=?");
          $get_items->bind_param("i", $id_application);
          $get_items->execute();
          $result = $get_items->get_result();


          $application_data = $result->fetch_array(MYSQLI_ASSOC);
          if ($location == 'order' || $location == 'offline') {
            $items = json_decode($application_data['items'])->{'items'};
            $edit_stock = $mysqli->prepare("INSERT INTO $table_two (goods_id, quantity, user_id) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE quantity=quantity+VALUES(quantity)");

          }
          if ($location == 'stock') {
            //print_r($application_data);
            $items = json_decode($application_data['items']);
            $edit_stock = $mysqli->prepare("INSERT INTO $table_two (goods_id, quantity, user_id) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE quantity=quantity-VALUES(quantity)");

          }
          if (isset($application->{'user_id'})) {
            $id = $application->{'user_id'};
            $edit_stock->bind_param("iii", $goods_id, $quantity, $id);
            foreach ($items as $key => $value) {
              $goods_id = $value->{'id'};
              $quantity = $value->{'quantity'};
              $edit_stock->execute();
            }

          }

        }


      }
    } else $status = 2;

    if ($location == 'offline') {
      //Если продажа офлайн иключаем статус "рассматривается"
      if ($application->{'status'} == 1) $status = 2;
      if ($application->{'status'} == 2) $status = 2;
    }

    $data['status'] = $status;


    $stmt = $mysqli->prepare("UPDATE $table SET `status`=? WHERE `id`=?");
    $stmt->bind_param("ii", $status, $id_application);
    $stmt->execute();
    if ($stmt) {
      $data['error'] = 0;
      if ($location = 'stock') {
        $subject = 'Отмена заявки № ' . $id_application;

        $message = '<html>
        <head>
          <title>' . $subject . '</title>
        </head>
        <body style=" font-family: sans-serif;">
        <table border="0" style="max-width: 100%; width: 600px; margin: 0 auto 45px;">
          <tr>
            <th style="color: #00bcf9; vertical-align: top; font-size: 38px; text-transform: uppercase; font-weight: 800; padding: 0; text-align: left;">
              <p style="line-height: 1; margin: 0; padding: 0;">Holiday<br> Paint</p>
            </th>
            <th style="width: 200px; vertical-align: top; text-align: right;">
              <img src="http://my.holiday-paint.ru/logo.png" alt="" style="max-width: 150px; height: auto;">
            </th>
          </tr>
        </table>




          <div style="background-color: #FFF; text-align: center; padding: 15px;">
          Заказ № ' . $id_application . ' был отменен

          </div>

          <div style="background-color: #00bcf9; text-align: center; padding: 15px;">
        </div>

        <a href="http://my.holiday-paint.ru/report" style="text-transform: uppercase; padding: 10px 20px; background-color: #00bcf9; color: #FFF; font-weight: 300; font-size: 24px; text-decoration: none; border-radius: 5px; margin: 45px auto; display: block; width: 200px; text-align: center;">HoliCloud</a>

        </body>
        </html>
        ';
        $to = 'zakaz@holiday-paint.ru';
        $headers = array();
        $headers  = 'MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=utf-8' . "\r\n";
        // Отправляем
        mail($to, $subject, $message, $headers);
      }
    }
    else  $data['error'] = 3;
    $stmt->close();
    $mysqli->close();
  }
} else  $data['error'] = 1;

echo json_encode($data);

?>
