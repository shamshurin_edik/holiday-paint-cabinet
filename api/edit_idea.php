<?php
if (!empty($_POST['data'])) {

  require_once 'config.php'; // подключаем скрипт
  $table = 'ideas';
  $idea = json_decode( $_POST['data'] );

  $user = $idea->{'author'};
  if(isset($idea->{'subject'})) $subject = $idea->{'subject'};
  else $subject = NULL;
  if(isset($idea->{'content'})) $content = $idea->{'content'};
  else $content = NULL;

  if (isset($idea->{'id'})) {
    $act = 'edit';
    $id = $idea->{'id'};
  } else {
    $act = 'new';
  }



  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    //echo $items;
    $mysqli->set_charset("utf8");

    if ($act == 'new') {
      $edit_idea = $mysqli->prepare("INSERT INTO $table (subject, content, author) VALUES (?, ?, ?)");
      $edit_idea->bind_param("ssi", $subject, $content, $user);
    }

    if ($act == 'edit') {
      $edit_idea = $mysqli->prepare("UPDATE $table SET subject=?, content=? WHERE id=?");
      $edit_idea->bind_param("ssi", $subject, $content, $id);
    }

    $edit = $edit_idea->execute();
    if ($edit) $data['error'] = 0;
    if ($act == 'new') $data['id'] = $mysqli->insert_id;
    $edit_idea->close();
    $mysqli->close();

  }

} else $data['error'] = 1;

echo json_encode($data);
?>
