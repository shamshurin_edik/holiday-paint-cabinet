<?php
if (!empty($_POST['data'])) {

  require_once 'config.php'; // подключаем скрипт
  $table = 'promo';
  $promo = json_decode( $_POST['data'] );

  if (isset($promo->{'id'})) {
    $act = 'edit';
    $id = $promo->{'id'};
  } else {
    $act = 'new';
  }

  $main_header = $promo->{'main_header'};
  $sub_header = $promo->{'sub_header'};
  $header = $promo->{'header'};
  $description = $promo->{'description'};
  if (false == ($code = $promo->{'code'})) $code = NULL;
  if (false == ($start = $promo->{'start'})) $start = NULL;
  if (false == ($finish = $promo->{'finish'})) $finish = NULL;

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    //echo $items;
    $mysqli->set_charset("utf8");

    if ($act == 'new') {
      $edit_promo = $mysqli->prepare("INSERT INTO $table (main_header, sub_header, header, description, code, start, finish) VALUES (?, ?, ?, ?, ?, ?, ?)");
      $edit_promo->bind_param("sssssss", $main_header, $sub_header, $header, $description, $code, $start, $finish);
    }

    if ($act == 'edit') {
      $edit_promo = $mysqli->prepare("UPDATE $table SET main_header=?, sub_header=?, header=?, description=?, code=?, start=?, finish=? WHERE id=?");
      $edit_promo->bind_param("sssssssi", $main_header, $sub_header, $header, $description, $code, $start, $finish, $id);
    }

    $edit = $edit_promo->execute();
    if ($edit) $data['error'] = 0;
    if ($act == 'new') $data['id'] = $mysqli->insert_id;
    $edit_promo->close();
    $mysqli->close();

  }

} else $data['error'] = 1;

echo json_encode($data);
?>
