<?php
if (!empty($_POST['data'])) {
  require_once 'config.php'; // подключаем скрипт
  $user = json_decode( $_POST['data'] );
  $id = $user->{'id'};
  $key = $user->{'key'};
  $table = 'users';

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    $mysqli->set_charset("utf8");

    $stmt = $mysqli->prepare("SELECT * FROM $table WHERE id=? AND password=?");
    $stmt->bind_param("is", $id, $key);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result) {
      $row = $result->fetch_assoc();
      if (!empty($row['id'])) {
        $data['error'] = 0;
        $data['level'] = $row['level'];
      } else {
        $data['error'] = 4;
      }
    } else $data['error'] = 3;

    $stmt->close();
    $mysqli->close();
  }

} else $data['error'] = 1;

echo json_encode($data);
?>
