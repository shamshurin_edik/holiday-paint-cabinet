<?php
require_once 'config.php'; // подключаем скрипт
$table = 'ideas';
$table_two = 'users_info';
$table_likes = 'likes';
$config = json_decode( $_POST['data'] );
$id = $config->{'user'};
$data = [];

$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
if ($mysqli->connect_errno) {
  //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
  $data['error'] = 2;
} else {
  $mysqli->set_charset("utf8");

  $type = 'idea';

  $stmt = $mysqli->prepare("SELECT $table.*, $table_two.name, $table_two.photo, $table_likes.action AS liked FROM $table LEFT JOIN $table_two ON $table.author = $table_two.id LEFT JOIN $table_likes ON $table_likes.obj_id = $table.id AND $table_likes.type = 'idea' AND $table_likes.user_id = ?");
  //$stmt = $mysqli->prepare("SELECT $table.*, $table_two.name, $table_two.photo FROM $table LEFT JOIN $table_two ON $table.author = $table_two.id");
  $stmt->bind_param("i", $id);

  $stmt->execute();
  $result = $stmt->get_result();

  $sum_likes = $mysqli->prepare("SELECT SUM(action) AS rating FROM $table_likes WHERE type=? AND obj_id=?");
  $sum_likes->bind_param("si", $type, $id_idea);

  while ($row = $result->fetch_assoc()) {
    $id_idea = $row['id'];
    $sum_likes->execute();
    $likes = $sum_likes->get_result();
    $likes_s = $likes->fetch_assoc();
    $row['rating'] = $likes_s['rating'];
    if (empty($likes_s['rating'])) $row['rating'] = 0;
    $data[] = $row;
  }
  $stmt->close();
  $mysqli->close();
  if (count($data) == 0) $data['error'] = 0;
}

echo json_encode($data);

?>
