<?php
if (!empty($_POST['data'])) {

  require_once 'config.php'; // подключаем скрипт
  $table = 'lessons';
  $lesson = json_decode( $_POST['data'] );

  $header = $lesson->{'header'};
  $aval = $lesson->{'aval'};
  if(isset($lesson->{'conditions'})) $conditions = $lesson->{'conditions'};
  else $conditions = NULL;
  if(isset($lesson->{'description'})) $description = $lesson->{'description'};
  else $description = NULL;
  $questions = json_encode($lesson->{'questions'});

  if (isset($lesson->{'id'})) {
    $act = 'edit';
    $id = $lesson->{'id'};
  } else {
    $act = 'new';
  }

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    //echo $items;
    $mysqli->set_charset("utf8");

    if ($act == 'new') {
      $edit_lesson = $mysqli->prepare("INSERT INTO $table (aval, conditions, header, description, questions) VALUES (?, ?, ?, ?, ?)");
      $edit_lesson->bind_param("sisss", $aval, $conditions, $header, $description, $questions);
    }

    if ($act == 'edit') {
      $edit_lesson = $mysqli->prepare("UPDATE $table SET aval=?, conditions=?, header=?, description=?, questions=? WHERE id=?");
      $edit_lesson->bind_param("sssssi", $aval, $conditions, $header, $description, $questions, $id);
    }

    $edit = $edit_lesson->execute();
    if ($edit) $data['error'] = 0;
    if ($act == 'new') $data['id'] = $mysqli->insert_id;
    $edit_lesson->close();
    $mysqli->close();

  }

} else $data['error'] = 1;

echo json_encode($data);
?>
