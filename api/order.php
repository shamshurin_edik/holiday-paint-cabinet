<?php
$data['error'] = 0;
if (!empty($_POST['data'])) {
  require_once 'config.php'; // подключаем скрипт
  $action = json_decode($_POST['data']);
  $id = ($action->{'id'});
  $act = ($action->{'action'});
  $location = ($action->{'location'});

  if ($location == 'offline') {

    $table = 'partner_offline_cart';
    $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

    if ($mysqli->connect_errno) {
      //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
      $data['error'] = 2;
    } else {

      $mysqli->set_charset("utf8");
      if (!empty($_POST['data'])) {
        $items = json_decode( $_POST['items'] );
      } else $data['error'] = 2;

      $table_two = 'stock';

      $sell = $mysqli->prepare("UPDATE $table_two SET goods_id=?, quantity=quantity-? WHERE goods_id=? AND user_id=?");
      $sell->bind_param("iiii", $item_id, $quantity, $item_id, $id);

      foreach ($items as $value) {
        $item_id = $value->{'id'};
        $quantity = $value->{'quantity'};
        $sell->execute();
      }

      $sell->close();

      if ($data['error'] == 0) {
        $sell_after = $mysqli->prepare("DELETE FROM $table WHERE user_id = ?");
        $sell_after->bind_param("i", $id);
        $sell_after->execute();
        $sell_after->close();
      }


      $table_application = 'applications';

      $items_json = json_encode($items);
      $status = 1;

      $order_detail = new \stdClass();
      $order_detail->{'items'} = $items;
      $order_detail->{'buyer'} = json_decode( $_POST['buyer'] );
      $json_order_detail = json_encode($order_detail);


      $save_application = $mysqli->prepare("INSERT INTO $table_application (user_id, items, status, action) VALUES (?, ?, ?, ?)");
      $save_application->bind_param("isis", $id, $json_order_detail, $status, $location);
      $save_application->execute();
      $save_application->close();

    }

  }

  $mysqli->close();


} else $data['error'] = 1;
echo json_encode($data);

 ?>
