<?php

require_once 'config.php'; // подключаем скрипт
$table = 'users_info';
$table_two = 'cities';
$table_three = 'users';

$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
if ($mysqli->connect_errno) {
  //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
  $data['error'] = 2;
} else {
  $mysqli->set_charset("utf8");

  $stmt = $mysqli->prepare("SELECT t1.*, a.*, t1.id AS id FROM $table t1, (SELECT city, MIN(last_sale) last_sale FROM $table WHERE id!=2 AND id!=3 AND id!=73 GROUP BY city) t2 INNER JOIN $table_two a ON a.id = t2.city WHERE t1.city=t2.city AND t1.last_sale=t2.last_sale ORDER BY a.name");
  $stmt->execute();
  $result = $stmt->get_result();
  $get_email = $mysqli->prepare("SELECT email, level FROM $table_three WHERE id=?");
  $get_email->bind_param("i", $user_id);

  while ($row = $result->fetch_assoc()) {
    $user_id = $row['id'];
    $get_email->execute();
    $result_email = $get_email->get_result();
    $email = $result_email->fetch_array(MYSQLI_ASSOC);
    $row['email'] = $email['email'];
    $data['cities'][] = $row;
  }
  $get_email->close();
  $stmt->close();
  $mysqli->close();
  if (count($data) == 0) $data['error'] = 0;
}

if (!empty($_POST['data'])) {
  $geo = json_decode( $_POST['data'] );
  $coordinates = ($geo->{'coordinates'});
  $lon = $coordinates[0];
  $lat = $coordinates[1];
  //echo $lon . $lat;
  $i = 0;
  $lon_r = 0;
  $lat_r = 0;
  $lon_r_d = 0;
  $lat_r_d = 0;
  foreach ($data['cities'] as $value) {
    //print_r($value);
    $lon_r_d = abs($lon - $value['longitude']);
    $lat_r_d = abs($lat - $value['latitude']);
    if ($i > 0) {
      if (($lon_r_d + $lat_r_d) < ($lon_r + $lat_r)) {
        $lon_r = $lon_r_d;
        $lat_r = $lat_r_d;
        $city = $value['id'];
      }
    } else {
      $lon_r = $lon_r_d;
      $lat_r = $lat_r_d;
      $city = $value['id'];
    }
    $i++;
  }
  $data['id'] = $city;
}

echo json_encode($data);


?>
