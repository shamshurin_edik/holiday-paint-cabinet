<?php
require_once 'config.php'; // подключаем скрипт

if (!empty($_POST['data'])) {
  $options = json_decode( $_POST['data'] );
  $category = ($options->{'category'});
} else $category = 0;

if ($category == 0) {
  $set_filter = "WHERE `status`=1";
} else {
  $set_filter = "WHERE `category`='$category' AND `status`=1";
}

$table = 'stencils';


$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
if ($mysqli->connect_errno) {
  //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
  $data['error'] = 1;
} else {
  $mysqli->set_charset("utf8");

  $goods = $mysqli->prepare("SELECT * FROM $table $set_filter ORDER BY `id` DESC");

  $get = $goods->execute();

  if ($get) {
    $result = $goods->get_result();
    $goods->close();
    $data = array();
    while ($row = $result->fetch_assoc()) {
      $data[] = $row;
    }
  } else $data['error'] = 2;

}

echo json_encode($data);

?>
