<?php
if (!empty($_POST['data'])) {
  require_once 'config.php'; // подключаем скрипт
  $user = json_decode( $_POST['data'] );
  $id = ($user->{'id'});
  $table = 'goods';
  $table_two = 'partner_offline_cart';
  $data = [];

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    $mysqli->set_charset("utf8");

    $stmt = $mysqli->prepare("SELECT $table.*, $table_two.quantity FROM $table INNER JOIN $table_two ON $table.id = $table_two.goods_id AND $table_two.user_id = ?");
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()) {
      $data[] = $row;
    }
    if (count($data) == 0) $data['error'] = 0;
  }
} else $data['error'] = 1;

echo json_encode($data);

?>
