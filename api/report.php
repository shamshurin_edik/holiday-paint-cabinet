<?php
require_once 'config.php'; // подключаем скрипт

$_monthsList = array(
"1"=>"Январь","2"=>"Февраль","3"=>"Март",
"4"=>"Апрель","5"=>"Май", "6"=>"Июнь",
"7"=>"Июль","8"=>"Август","9"=>"Сентябрь",
"10"=>"Октябрь","11"=>"Ноябрь","12"=>"Декабрь");

$month = $_monthsList[date("n")];
$prev_month = $_monthsList[date("n", strtotime('now -1 month'))];


if (!empty($_POST['data'])) {
  $options = json_decode( $_POST['data'] );

  $user = ($options->{'user_id'});



  $table = 'applications';
  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    $mysqli->set_charset("utf8");
    $get_order_month = $mysqli->prepare("SELECT `action`,`items` FROM $table WHERE `user_id`=? AND month(date) = month(now()) and year(date) = year(now()) ORDER BY `id` DESC");
    $get_order_month->bind_param("i", $user);

    $get = $get_order_month->execute();


    $result = $get_order_month->get_result();
    $data = array();
    $sum_month = 0;

    $table_items = 'goods';
    $get_price = $mysqli->prepare("SELECT price FROM $table_items WHERE id=?");
    $get_price->bind_param("i", $id_item);

    while ($row = $result->fetch_assoc()) {
      if ($row['action'] == 'order') {
        $items = json_decode($row['items'])->{'items'};
        foreach ($items as $key => $value) {

          $id_item = $value->{'id'};
          $get_price->execute();
          $result_price= $get_price->get_result();
          $price = $result_price->fetch_assoc();

          $sum_month = $sum_month + $value->{'quantity'} * $price['price'];

          if (isset(json_decode($row['items'])->{'discount'})) {
            $discount = json_decode($row['items'])->{'discount'};
            $sum_month = $sum_month - $discount->{'discount'};
          }
        }
      } elseif ($row['action'] == 'offline') {
        $items = json_decode($row['items'])->{'items'};
        foreach ($items as $key => $value) {
          $sum_month = $sum_month + $value->{'quantity'} * $value->{'price'};
        }
      }
    }

    $get_order_month->close();





    $get_order_prev_month = $mysqli->prepare("SELECT `action`,`items` FROM $table WHERE `user_id`=? AND date > LAST_DAY(DATE_SUB(CURDATE(), INTERVAL 2 MONTH)) AND date < DATE_ADD(LAST_DAY(CURDATE() - INTERVAL 1 MONTH), INTERVAL 1 DAY) ORDER BY `id` DESC");
    $get_order_prev_month->bind_param("i", $user);

    $get = $get_order_prev_month->execute();

    $result = $get_order_prev_month->get_result();
    $data = array();
    $sum_prev_month = 0;
    while ($row = $result->fetch_assoc()) {
      if ($row['action'] == 'order') {
        $items = json_decode($row['items'])->{'items'};
        foreach ($items as $key => $value) {

          $id_item = $value->{'id'};
          $get_price->execute();
          $result_price= $get_price->get_result();
          $price = $result_price->fetch_assoc();

          $sum_prev_month = $sum_prev_month + $value->{'quantity'} * $price['price'];

          if (isset(json_decode($row['items'])->{'discount'})) {
            $discount = json_decode($row['items'])->{'discount'};
            $sum_prev_month = $sum_prev_month - $discount->{'discount'};
          }

        }
      } elseif ($row['action'] == 'offline') {
        $items = json_decode($row['items']);
        foreach ($items as $key => $value) {
          $sum_prev_month = $sum_prev_month + $value->{'quantity'} * $value->{'price'};
        }
      }

    }

    $get_order_prev_month->close();
    $data['month'] = [$month, $sum_month, $prev_month, $sum_prev_month];


    $action = 'stock';
    $get_last_prder = $mysqli->prepare("SELECT * FROM $table WHERE `user_id`=? AND `action`<>? AND `status`=1 ORDER BY `id` DESC LIMIT 3");
    $get_last_prder->bind_param("is", $user, $action);
    $get_last_prder->execute();
    $result = $get_last_prder->get_result();

    while ($row = $result->fetch_assoc()) {


      $sum = 0;
      if ($row['action'] == 'order') {
        $items = json_decode($row['items'])->{'items'};
        foreach ($items as $key => $value) {

          $id_item = $value->{'id'};
          $get_price->execute();
          $result_price= $get_price->get_result();
          $price = $result_price->fetch_assoc();

          $sum = $sum + $value->{'quantity'} * $price['price'];
        }
      } else if ($row['action'] == 'offline') {
        $items = json_decode($row['items'])->{'items'};
        foreach ($items as $key => $value) {
          $sum = $sum + $value->{'quantity'} * $value->{'price'};
        }
      }
      $row['sum'] = $sum;
      if (isset(json_decode($row['items'])->{'discount'})) {
        $discount = json_decode($row['items'])->{'discount'};
        $row['sum'] = $row['sum'] - $discount->{'discount'};
      }

      $data['last_order'][] = $row;
    }
    $get_last_prder->close();





    $table = 'goods';
    $table_two = 'stock';

    $stock = $mysqli->prepare("SELECT $table.*, $table_two.quantity FROM $table INNER JOIN $table_two ON $table.id = $table_two.goods_id AND $table_two.user_id = ?");
    $stock->bind_param("i", $user);

    $stock->execute();
    $result = $stock->get_result();
    $sum_stock = 0;
    while ($row = $result->fetch_assoc()) {
      $sum_stock = $sum_stock + $row['quantity'];
      if ($row['quantity'] < 10 && $row['quantity'] !== 0) $data['stock'][] = $row;
    }
    $data['sum_stock'] = $sum_stock;
    $stock->close();


    $table_news = 'news';
    $last_news = $mysqli->prepare("SELECT * FROM $table_news ORDER BY `id` DESC LIMIT 1");
    $last_news->execute();
    $result_news = $last_news->get_result();
    $news = $result_news->fetch_assoc();
    $data['last_news'] = $news;
    $last_news->close();


    $mysqli->close();

  }
}

  echo json_encode($data);
?>
