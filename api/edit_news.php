<?php
if (!empty($_POST['data'])) {

  require_once 'config.php'; // подключаем скрипт
  $table = 'news';
  $news = json_decode( $_POST['data'] );

  $header = $news->{'header'};
  $description = $news->{'description'};


  if (isset($news->{'id'})) {
    $act = 'edit';
    $id = $news->{'id'};
  } else {
    $act = 'new';
  }

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    //echo $items;
    $mysqli->set_charset("utf8");

    if ($act == 'new') {
      $edit_news = $mysqli->prepare("INSERT INTO $table (header, description) VALUES (?, ?)");
      $edit_news->bind_param("ss", $header, $description);
    }

    if ($act == 'edit') {
      $edit_news = $mysqli->prepare("UPDATE $table SET header=?, description=? WHERE id=?");
      $edit_news->bind_param("ssi", $header, $description, $id);
    }

    $edit = $edit_news->execute();
    if ($edit) $data['error'] = 0;
    if ($act == 'new') $data['id'] = $mysqli->insert_id;
    $edit_news->close();
    $mysqli->close();

  }

} else $data['error'] = 1;

echo json_encode($data);
?>
