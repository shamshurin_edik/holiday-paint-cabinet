<?php
require_once 'config.php'; // подключаем скрипт
$table = 'stencils_categories';
$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD)
  or die('Не удалось соединиться: ' . mysqli_error());
mysqli_select_db($link, DB_NAME) or die('Не удалось выбрать базу данных');
mysqli_set_charset($link, 'utf8');
$query = "SELECT * FROM $table";
$result = mysqli_query($link, $query);

$data = array();
while ($categories = mysqli_fetch_assoc($result)) {
  $data[] = $categories;
}

mysqli_close($link);
echo json_encode($data);

?>
