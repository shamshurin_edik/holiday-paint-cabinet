<?php
require_once 'config.php'; // подключаем скрипт
$table = 'users_info';

$table_two = 'cities';

$table_three = 'users';

$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
if ($mysqli->connect_errno) {
  //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
  $data['error'] = 2;
} else {
  $mysqli->set_charset("utf8");


  if (isset($_POST['data'])) {
    $config = json_decode( $_POST['data'] );
    if(isset($config->{'page'})) {
      $page = $config->{'page'} - 1;
      $limit = $config->{'limit'};
      $get_partners = $mysqli->prepare("SELECT $table.*, $table_two.name AS city FROM $table INNER JOIN $table_two ON $table.city = $table_two.id ORDER BY $table.city DESC LIMIT ?, ?");
      $get_partners->bind_param("ii", $page, $limit);

    } else $page = false;


    if(isset($config->{'top'})) {
      $top = $config->{'top'};
      if ($top == 'points') {
        if ($page !== false) {
          $get_partners = $mysqli->prepare("SELECT $table.*, $table_two.name AS city FROM $table INNER JOIN $table_two ON $table.city = $table_two.id WHERE $table.id!=2 AND $table.id!=3 AND $table.id!=73 ORDER BY $table.points DESC LIMIT ?, ?");
          $get_partners->bind_param("ii", $page, $limit);
        } else {
          $get_partners = $mysqli->prepare("SELECT $table.*, $table_two.name AS city FROM $table INNER JOIN $table_two ON $table.city = $table_two.id WHERE $table.id!=2 AND $table.id!=3 AND $table.id!=73 ORDER BY $table.points DESC");

        }
      }

    } else $top = false;

    if(isset($config->{'city'})) {
      $city = $config->{'city'};
      $get_partners = $mysqli->prepare("SELECT $table.*, $table_two.name AS city FROM $table INNER JOIN $table_two ON $table.city = $table_two.id AND $table.city = ? ORDER BY $table.points DESC");
      $get_partners->bind_param("i", $city);


    } else $city = false;




  } else {
    $get_partners = $mysqli->prepare("SELECT $table.*, $table_two.name AS city, $table_three.level, $table_three.email FROM $table INNER JOIN $table_two ON $table.city = $table_two.id INNER JOIN $table_three ON $table_three.id = $table.id ORDER BY $table.city DESC");
  }


  $get = $get_partners->execute();


  $result = $get_partners->get_result();
  $data = array();
  while ($row = $result->fetch_assoc()) {
    $data[] = $row;
  }

  $get_partners->close();
  $mysqli->close();

}

echo json_encode($data);

?>
