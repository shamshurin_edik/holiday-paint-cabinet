<?php
public function send_email($to='shamshurin.edik@gmail.com',$subject='Тема письма') {

  $message = '
  <html>
  <head>
    <title>Birthday Reminders for August</title>
  </head>
  <body>
    <p>Here are the birthdays upcoming in August!</p>
    <table>
      <tr>
        <th>Person</th><th>Day</th><th>Month</th><th>Year</th>
      </tr>
      <tr>
        <td>Johny</td><td>10th</td><td>August</td><td>1970</td>
      </tr>
      <tr>
        <td>Sally</td><td>17th</td><td>August</td><td>1973</td>
      </tr>
    </table>
  </body>
  </html>
  ';

  // Для отправки HTML-письма должен быть установлен заголовок Content-type
  $headers  = 'MIME-Version: 1.0' . "\r\n";
  $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

  // Дополнительные заголовки
//  $headers[] = 'To: Mary <mary@example.com>, Kelly <kelly@example.com>';
  $headers[] = 'From: Шамшурин Эдик <shamshurin.edik@gmail.com>';
//  $headers[] = 'Cc: birthdayarchive@example.com';
//  $headers[] = 'Bcc: birthdaycheck@example.com';

  // Отправляем
  mail($to, $subject, $message, implode("\r\n", $headers));
}
?>
