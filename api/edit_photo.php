<?php
if (!empty($_POST['data'])) {
  $photo = json_decode( $_POST['data'] );
  $id = $photo->{'id'};
  $action = $photo->{'action'};

  require_once 'config.php';
  $table = 'photos';

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    $mysqli->set_charset("utf8");

    if ($action == 'set_status') {
      $edit_photo = $mysqli->prepare("UPDATE $table SET status = IF ( status=1, 0, 1 ) WHERE id=?");
      $edit_photo->bind_param("i", $id);
      $edit = $edit_photo->execute();
      $edit_photo->close();
      if ($edit) $data['error'] = 0;
    }

    if ($action == 'remove') {
      $edit_photo = $mysqli->prepare("DELETE FROM $table WHERE id = ?");
      $edit_photo->bind_param("i", $id);
      $edit = $edit_photo->execute();
      $edit_photo->close();
      $name = $photo->{'name'};
      $dir = 'media/photos/';
      unlink($dir . $name);
      unlink($dir . "th_" . $name);
      if ($edit) $data['error'] = 0;
    }

    $mysqli->close();
  }
} else $data['error'] = 1;

echo json_encode($data);
?>
