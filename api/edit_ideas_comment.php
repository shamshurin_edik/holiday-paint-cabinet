<?php
if (!empty($_POST['data'])) {

  require_once 'config.php'; // подключаем скрипт
  $table = 'ideas_comments';
  $comment = json_decode( $_POST['data'] );

  $text = $comment->{'text'};

  if (isset($comment->{'id'})) {
    $act = 'edit';
    $id = $comment->{'id'};
  } else {
    $act = 'new';
    $author = $comment->{'author'};
    $idea = $comment->{'idea'};
  }



  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    //echo $items;
    $mysqli->set_charset("utf8");

    if ($act == 'new') {
      $edit_comment = $mysqli->prepare("INSERT INTO $table (idea, author, text) VALUES (?, ?, ?)");
      $edit_comment->bind_param("iis", $idea, $author, $text);
    }

    if ($act == 'edit') {
      $edit_comment = $mysqli->prepare("UPDATE $table SET text=? WHERE id=?");
      $edit_comment->bind_param("si", $text, $id);
    }

    $edit = $edit_comment->execute();
    if ($edit) $data['error'] = 0;
    if ($act == 'new') $data['id'] = $mysqli->insert_id;
    $edit_comment->close();
    $mysqli->close();

  }

} else $data['error'] = 1;

echo json_encode($data);
?>
