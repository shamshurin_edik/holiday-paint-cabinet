<?php
require_once 'config.php'; // подключаем скрипт

if (!empty($_POST['data'])) {
  $options = json_decode( $_POST['data'] );
  $category = ($options->{'category'});
  if(isset($options->{'user'})) {
    $user = ($options->{'user'});
  } else $user = false;
  if(isset($options->{'cart'})) {
    $cart = ($options->{'cart'});
  } else $cart = false;
} else $category = 0;

if ($category == 0) {
  $set_filter = " WHERE `status`=1";
} else {
  $set_filter = " WHERE `status`=1 AND `category`='$category' ";
}

$table = 'goods';


$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
if ($mysqli->connect_errno) {
  //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
  $data['error'] = 1;
} else {
  $mysqli->set_charset("utf8");

  $goods = $mysqli->prepare("SELECT * FROM $table $set_filter ORDER BY `category` DESC");


  if ($user) {
    $table_two = 'partner_cart';
    //если есть id пользователя, то добавить корзину.
    $goods = $mysqli->prepare("SELECT $table.*, $table_two.quantity FROM $table LEFT JOIN $table_two ON $table.id = $table_two.goods_id AND $table_two.user_id = ?". $set_filter);

    if ($cart) {
      $goods = $mysqli->prepare("SELECT $table.*, $table_two.quantity FROM $table INNER JOIN $table_two ON $table.id = $table_two.goods_id AND $table_two.user_id = ?". $set_filter);
    }
    $goods->bind_param("i", $user);
  }

  $get = $goods->execute();

  if ($get) {
    $result = $goods->get_result();
    $goods->close();
    $data = array();
    while ($row = $result->fetch_assoc()) {
      $data[] = $row;
    }
  } else $data['error'] = 2;

}

echo json_encode($data);

?>
