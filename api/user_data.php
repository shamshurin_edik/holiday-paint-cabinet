<?php
if (!empty($_POST['data'])) {
  require_once 'config.php'; // подключаем скрипт
  $user = json_decode( $_POST['data'] );
  $id = ($user->{'id'});
  $data = [];

  $table = 'users_info';
  $table_two = 'cities';

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    $mysqli->set_charset("utf8");
    $stmt = $mysqli->prepare("SELECT $table.*, $table_two.name AS city FROM $table INNER JOIN $table_two ON $table.city = $table_two.id AND $table.id = ?");
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $result = $stmt->get_result();
    $data = $result->fetch_assoc();
    //if (!$data) $data['empty'] = true;
    //print_r($row);
    $stmt->close();

  }
  $mysqli->close();

} else $data['error'] = 1;

echo json_encode($data);

?>
