<?php
require_once 'config.php'; // подключаем скрипт
$table = 'promo';
$data = [];

$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
if ($mysqli->connect_errno) {
  //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
  $data['error'] = 2;
} else {
  $mysqli->set_charset("utf8");

  $stmt = $mysqli->prepare("SELECT * FROM $table");
  $stmt->execute();
  $result = $stmt->get_result();

  $row_cnt = $result->num_rows;
  $data['error'] = 0;
  $data['quantity'] = $row_cnt;

  $stmt->close();
  $mysqli->close();
}

echo json_encode($data);

?>
