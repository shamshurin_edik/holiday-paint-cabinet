<?php
$uploaddir = 'users/';
$extension = 'png';
$filename = uniqid() . '.' . $extension;
$uploadfile = $uploaddir . $filename;


function resize($file_input, $file_output, $w_o, $h_o) {
  list($w_i, $h_i, $type) = getimagesize($file_input);
  if (!$w_i || !$h_i) {
    echo 'Невозможно получить длину и ширину изображения при уменьшении';
    return;
  }
  $types = array('','gif','jpeg','png');
  $ext = $types[$type];
  if ($ext) {
    $func = 'imagecreatefrom'.$ext;
    $img = $func($file_input);
  } else {
    echo 'Некорректный формат файла';
    return;
  }
  if (!$h_o) $h_o = $w_o/($w_i/$h_i);
  if (!$w_o) $w_o = $h_o/($h_i/$w_i);
  $img_o = imagecreatetruecolor($w_o, $h_o);
  imageAlphaBlending($img_o, false); //не смешиваем, а заменяем
  imageSaveAlpha($img_o,true);
  imagecopyresampled($img_o, $img, 0, 0, 0, 0, $w_o, $h_o, $w_i, $h_i);
  if ($type == 2) {
    return imagejpeg($img_o,$file_output,100);
  } else {
    $func = 'image'.$ext;
    return $func($img_o,$file_output);
  }
 }


if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
  $data['name'] = $filename;


  $nw = 150;    // Ширина миниатюр
  $nh = 150;    // Высота миниатюр
  $dest = $uploaddir . "th_" . $filename;   // Файл с результатом работы
  resize($uploadfile, $dest, '', $nh);


  require_once 'config.php'; // подключаем скрипт
  $table = 'users_info';
  $link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD)
    or die('Не удалось соединиться: ' . mysqli_error());
  mysqli_select_db($link, DB_NAME) or die('Не удалось выбрать базу данных');
  mysqli_set_charset($link, 'utf8');

  $id = mysqli_real_escape_string($link, $_FILES['image']['name']);

  $query = "UPDATE $table SET `photo`='$filename' WHERE `id`='$id'";
  $result = mysqli_query($link, $query) or die('Запрос не удался: ' . mysqli_error());
  if ($result) $data['error'] = 0;
  else $data['error'] = 1;
  mysqli_close($link);

} else {
  $data['error'] = 1;
}

echo json_encode($data);
?>
