<?php
if (!empty($_POST['data'])) {
  require_once 'config.php'; // подключаем скрипт
  $user = json_decode( $_POST['data'] );
  $table = 'customers';


  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    $mysqli->set_charset("utf8");

    if(isset($user->{'id'}) AND isset($user->{'key'})) {
      $id = $user->{'id'};
      $key = $user->{'key'};
      $stmt = $mysqli->prepare("SELECT * FROM $table WHERE id=? AND `key`=?");
      $stmt->bind_param("is", $id, $key);
      $stmt->execute();
      $result = $stmt->get_result();
      if ($result) {

        $row = $result->fetch_assoc();
        if (!empty($row['id'])) {
          $data['id'] = $id;
          $data['key'] = $key;
          $data['error'] = 0;
        } else {
          $key = uniqid();
          $stmt = $mysqli->prepare("INSERT INTO $table(`key`) VALUES (?)");
          $stmt->bind_param("s", $key);
          $stmt->execute();
          $data['id'] = $mysqli->insert_id;
          $data['key'] = $key;
          $data['error'] = 0;
        }
      }
    } else {
      $key = uniqid();
      $stmt = $mysqli->prepare("INSERT INTO $table(`key`) VALUES (?)");
      $stmt->bind_param("s", $key);
      $stmt->execute();
      $data['id'] = $mysqli->insert_id;
      $data['key'] = $key;
      $data['error'] = 0;
    }
    $stmt->close();
    $mysqli->close();
  }


} else $data['error'] = 1;

echo json_encode($data);
?>
