<?php
require_once 'config.php'; // подключаем скрипт
$partner = false;
$admin = false;
$hashtag = false;
if (isset($_POST['data'])) {
  $config = json_decode( $_POST['data'] );
  if(isset($config->{'user'})) {
    if ($config->{'user'} == 'partner') {
      $admin = true;
    } else {
      $id = $config->{'user'};
      $partner = true;
    }
  }
  if (isset($config->{'hashtag'})) {
    $hashtag = $config->{'hashtag'};
    $mask = ['_','_','_','_','_','_','_'];
    $mask[$hashtag-1] = '1';
    $tag = implode("_", $mask);
  }
}
$table = 'photos';
$data = [];

$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
if ($mysqli->connect_errno) {
  //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
  $data['error'] = 2;
} else {
  $mysqli->set_charset("utf8");

  if ($admin) {
    $stmt = $mysqli->prepare("SELECT * FROM $table");
  } else {
    if ($hashtag) {
      $stmt = $mysqli->prepare("SELECT * FROM $table WHERE status=1 AND hashtags LIKE ?");
      $stmt->bind_param("s", $tag);
    } else {
      if ($partner) {
        $stmt = $mysqli->prepare("SELECT * FROM $table WHERE status=1 OR user_id=?");
        $stmt->bind_param("i", $id);
      } else {
        $stmt = $mysqli->prepare("SELECT * FROM $table WHERE status=1");
      }
    }
  }
  $stmt->execute();
  $result = $stmt->get_result();
  while ($row = $result->fetch_assoc()) {
    $data[] = $row;
  }
  $stmt->close();
  $mysqli->close();
  if (count($data) == 0) $data['error'] = 0;
}

echo json_encode($data);

?>
