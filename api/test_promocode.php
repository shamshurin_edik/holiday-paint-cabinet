<?php
require_once 'config.php'; // подключаем скрипт
$config = json_decode( $_POST['data'] );
if (isset($config->{'code'})) {
  $code = mb_strtolower($config->{'code'});
  $table = 'promocodes';
  $table_two = 'goods';
  $data = [];

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    $mysqli->set_charset("utf8");

    $stmt = $mysqli->prepare("SELECT * FROM $table WHERE code=?");
    $stmt->bind_param("s", $code);

    $stmt->execute();
    $result = $stmt->get_result();
    $code_row = $result->fetch_array(MYSQLI_ASSOC);
    if (isset($code_row['id'])) {
      $data['promo'] = json_decode($code_row['detail']);
      $data['error'] = 0;
      if($data['promo']->{'groupe'} == 'category') {
        $category = $data['promo']->{'category'};
        $goods = $mysqli->prepare("SELECT id FROM $table_two WHERE category=?");
        $goods->bind_param("i", $category);
        $get = $goods->execute();
        if ($get) {
          $result = $goods->get_result();
          $goods->close();
          $data['items'] = array();
          while ($row = $result->fetch_assoc()) {
            $data['items'][] = $row;
          }
        } else $data['error'] = 2;

      }
    } else {
      $data['error'] = 3;
    }
    $stmt->close();
    $mysqli->close();
    if (count($data) == 0) $data['error'] = 0;
  }
} else $data['error'] = 1;

echo json_encode($data);

?>
