<?php
if (!empty($_POST['data'])) {
  require_once 'config.php'; // подключаем скрипт
  $user = json_decode( $_POST['data'] );
  $id = ($user->{'id'});
  $table = 'shop_cart';
  if (!empty($_POST['items'])) {
    $items = json_decode( $_POST['items'] );

    $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
    if ($mysqli->connect_errno) {
      //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
      $data['error'] = 2;
    } else {
      $mysqli->set_charset("utf8");

      $remove = $mysqli->prepare("DELETE FROM $table WHERE user_id=?");
      $remove->bind_param("i", $id);
      $remove->execute();

      $stmt = $mysqli->prepare("INSERT INTO $table (user_id, goods_id, quantity) VALUES (?,?,?) ON DUPLICATE KEY UPDATE quantity=?");
      $stmt->bind_param("iiii", $id, $goods_id, $quantity, $quantity);
      if (count($items) == 0) $data['error'] = 0;
      else {
        foreach($items as $item) {
          $goods_id = $item->{'id'};
          $quantity = $item->{'quantity'};
          $stmt->execute();
          if ($stmt) $data['error'] = 0;
          else $data['error'] = 3;
        }
      }
      $remove->close();
      $stmt->close();
      $mysqli->close();
    }

  } else $data['error'] = 1;

} else $data['error'] = 1;

echo json_encode($data);

?>
