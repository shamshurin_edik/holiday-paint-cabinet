<?php
if (!empty($_POST['data'])) {
  require_once 'config.php'; // подключаем скрипт
  $order = json_decode( $_POST['data'] );
  $id = $order->{'id'};
  $name = $order->{'name'};
  $city = $order->{'city'};
  $phone = $order->{'phone'};
  $partner = $order->{'partner'};
  $shipping = $order->{'shipping'};
  $items = $order->{'items'};
  $partner_phone = $order->{'partner_phone'};
  if (isset($order->{'discount'})) {
    $discount = $order->{'discount'};
  }
  $email = $order->{'email'};
  if ($shipping == 1) {
    $adres = $order->{'adres'};
  } else $adres = false;

  $data['error'] = 0;
  $itms = array();
  foreach ($items as $key => $value) {
    $itm = new \stdClass();
    $itm->{'id'} = $value->{'id'};
    $itm->{'quantity'} = $value->{'quantity'};
    $itms[] = $itm;
  }
  $buyer = new \stdClass();
  $buyer->{'name'} = $name;
  $buyer->{'city'} = $city;
  $buyer->{'phone'} = $phone;
  $buyer->{'shipping'} = $shipping;
  $buyer->{'email'} = $email;
  if ($adres) $buyer->{'adres'} = $adres;

  $order_detail = new \stdClass();
  $order_detail->{'items'} = $itms;
  $order_detail->{'buyer'} = $buyer;
  if (isset($order->{'discount'})) {
    $order_detail->{'discount'} = $discount;
  }
  $json_order_detail = json_encode($order_detail);
  $location= 'order';
  $table = 'applications';
  $table_two = 'shop_cart';

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    //echo $items;
    $mysqli->set_charset("utf8");
    $save_application = $mysqli->prepare("INSERT INTO $table (user_id, items, action) VALUES (?, ?, ?)");
    $save_application->bind_param("iss", $partner,$json_order_detail, $location);
    $insert = $save_application->execute();
    $save_application->close();
    if ($insert) {
      $n_application = $mysqli->insert_id;
      $clear_cart = $mysqli->prepare("DELETE FROM $table_two WHERE user_id = ?");
      $clear_cart->bind_param("i", $id);
      $clear = $clear_cart->execute();
      $clear_cart->close();
      if ($clear) $data['error'] = 0;
      else $data['error'] = 4;

      //Заявка сохранена, корзина почищена
      //ОСталось разослать письма нужным людям
      //По основному адресу
      //Указанаму партнёру
      //Клиенту



      $partners_table = 'users';
      $partner_data = $mysqli->prepare("SELECT email FROM $partners_table WHERE id = ?");
      $partner_data->bind_param("i", $partner);
      $partner_data->execute();
      $result = $partner_data->get_result();
      $partner_row = $result->fetch_array(MYSQLI_ASSOC);
      $email_partner = $partner_row['email'];

      $goods_table = 'goods';
      $get_goods = $mysqli->prepare("SELECT name, price FROM $goods_table WHERE id = ?");
      $get_goods->bind_param("i", $id_goods);

      $order_items = array();

      $table = '';
      $all_price = 0;
      foreach ($items as $key => $value) {
        $id_goods = $value->{'id'};
        $get_goods->execute();
        $result = $get_goods->get_result();
        $goors_row = $result->fetch_array(MYSQLI_ASSOC);

        $itms = new \stdClass();
        $itms->{'name'} = $goors_row['name'];
        $itms->{'price'} = $goors_row['price'];
        $itms->{'quantity'} = $value->{'quantity'};
        $itms->{'sum'} = $value->{'quantity'} * $goors_row['price'];
        $all_price = $all_price + $itms->{'sum'};
        $order_items[] = $itms;


        $table .= '<tr>
                <th style="font-weight: 300; text-align: left;">' . $itms->{"name"} . '</th>
                <th style="font-weight: 300; text-align: left; width:80px;">' . $itms->{"price"} . 'р.</th>
                <th style="font-weight: 300; width:80px;">' . $itms->{"quantity"} . '</th>
                <th style="font-weight: 300; text-align: right; width:80px;">' . $itms->{"sum"} . 'р.</th>
              </tr>';

      }

      $to = $email_partner . ',zakaz@holiday-paint.ru'; // обратите внимание на запятую
      $client = $email;
      //$to = 'shamshurin.edik@gmail.com'; // обратите внимание на запятую

      if (isset($order->{'discount'})) {
        $disconts = '<br>скидка: ' . $discount->{'discount'} . ' промокод ' .  $discount->{'promocode'};
      } else $disconts = '';


      // тема письма
      $subject = 'Holiday-Paint - Заявка с сайта';
      $subject_client = 'Holiday-Paint - Ваш заказ';




$message = '<html>
<head>
  <title>Holiday-Paint - Заявка с сайта</title>
</head>
<table border="0" style="max-width: 100%; width: 600px; margin: 0 auto 45px;">
  <tr>
    <th style="color: #00b8f6; vertical-align: top; font-size: 38px; text-transform: uppercase; font-weight: 800; padding: 0; text-align: left;">
      <p style="line-height: 1; margin: 0; padding: 0;">Holiday<br> Paint</p>
    </th>
    <th style="width: 200px; vertical-align: top; text-align: right;">
      <img src="http://my.holiday-paint.ru/logo.png" alt="" style="max-width: 150px; height: auto;">
    </th>
  </tr>
</table>

  <table border="0" style="max-width: 100%; width: 600px; text-align: left; margin: 0 auto 15px;">
    <tr>
      <th>
        <p style="color: #00b8f6; vertical-align: top; font-size: 24px; text-transform: uppercase; margin: 0;">г. ' . $city . '</p>
        <p style="font-weight: 300; margin: 0; color: #999;">' . $adres . '</p>
      </th>
      <th style="width: 100px; vertical-align: top; font-weight: 300; color: #999;">Дата заказа</th>
      <th style="width: 70px; vertical-align: top; color: #00b8f6; vertical-align: top; font-size: 16px;">' . date("d/m/y") . '</th>
    </tr>
    <tr>
      <th>
      </th>
      <th style="width: 100px; vertical-align: top; font-weight: 300; color: #999;">Номер заказа</th>
      <th style="width: 70px; vertical-align: top; color: #00b8f6; vertical-align: top; font-size: 16px;">' . $n_application . '</th>
    </tr>

  </table>


  <div style="background-color: #00b8f6; text-align: center; padding: 15px; margin: 0 auto; max-width: 620px;">

    <table border="0" style="padding: 10px 0; border-bottom: 2px solid rgb(114, 211, 240); font-size: 14px; max-width: 100%; width: 600px; margin: 0 auto; color: #FFF; text-transform: uppercase;">
      <tr>
        <th style="font-weight: 300; text-align: left;">Наименование</th>
        <th style="font-weight: 300; text-align: left; width:80px;">Цена</th>
        <th style="font-weight: 300; width:80px;">Кол-во</th>
        <th style="font-weight: 300; text-align: right; width:80px;">Сумма</th>
      </tr>
    </table>

    <table border="0" style="padding: 10px 0; border-bottom: 2px solid rgb(114, 211, 240); font-size: 14px; max-width: 100%; width: 600px; margin: 0 auto; color: #FFF; text-transform: uppercase;">
      ' . $table . '
    </table>


    <table border="0" style="padding: 20px 0 10px;  font-size: 14px; max-width: 100%; width: 600px; margin: 0 auto; color: #FFF; text-transform: uppercase;">
      <tr>
        <th style="text-align: left;">
          <a style="font-size: 20px; font-weight: 700; padding: 10px 25px; background-color: rgb(81, 254, 83); border-radius: 5px; color: #FFF; text-decoration: none;" href="tel:' . $phone . '">Позвонить</a>
        </th>

        <th style="font-weight: 300; text-align: right; width:180px;">
          Итого <span style="font-size: 24px; font-weight: 700;">' . $all_price . ' р.</span>
          '. $disconts . '
        </th>
      </tr>

    </table>



  </div>

  <table border="0" style="padding: 20px 0 10px;  font-size: 14px; max-width: 100%; width: 600px; margin: 0 auto; color: #999; text-transform: none;">
    <tr>
      <th style="text-align: left; font-weight: 300;">
        Имя: ' . $name . '<br>
        Телефон: ' . $phone . '<br>
        E-Mail: ' . $email . '
      </th>

    </tr>

  </table>

  <div style="background-color: #00b8f6; text-align: center; padding: 15px;">
</div>

<a href="http://my.holiday-paint.ru/order" style="text-transform: uppercase; padding: 10px 20px; background-color: #00bcf9; color: #FFF; font-weight: 300; font-size: 24px; text-decoration: none; border-radius: 5px; margin: 45px auto; display: block; width: 200px; text-align: center;">HoliCloud</a>

</body>
</html>
';


$message_client = '<html>
<head>
  <title>Holiday-Paint - Заявка с сайта</title>
</head>
<body style=" font-family: sans-serif;">
<table border="0" style="max-width: 100%; width: 600px; margin: 0 auto 45px;">
  <tr>
    <th style="color: #00b8f6; vertical-align: top; font-size: 38px; text-transform: uppercase; font-weight: 800; padding: 0; text-align: left;">
      <p style="line-height: 1; margin: 0; padding: 0;">Holiday<br> Paint</p>
    </th>
    <th style="width: 200px; vertical-align: top; text-align: right;">
      <img src="http://my.holiday-paint.ru/logo.png" alt="" style="max-width: 150px; height: auto;">
    </th>
  </tr>
</table>

  <table border="0" style="max-width: 100%; width: 600px; text-align: left; margin: 0 auto 15px;">
    <tr>
      <th>
        <p style="color: #00b8f6; vertical-align: top; font-size: 24px; text-transform: uppercase; margin: 0;">г. ' . $city . '</p>
        <p style="font-weight: 300; margin: 0; color: #999;">' . $adres . '</p>
      </th>
      <th style="width: 100px; vertical-align: top; font-weight: 300; color: #999;">Дата заказа</th>
      <th style="width: 70px; vertical-align: top; color: #00b8f6; vertical-align: top; font-size: 16px;">' . date("d/m/y") . '</th>
    </tr>
    <tr>
      <th>
      </th>
      <th style="width: 100px; vertical-align: top; font-weight: 300; color: #999;">Номер заказа</th>
      <th style="width: 70px; vertical-align: top; color: #00b8f6; vertical-align: top; font-size: 16px;">' . $n_application . '</th>
    </tr>
  </table>


  <div style="background-color: #00b8f6; text-align: center; padding: 15px; margin: 0 auto; max-width: 620px;">

    <table border="0" style="padding: 10px 0; border-bottom: 2px solid rgb(114, 211, 240); font-size: 14px; max-width: 100%; width: 600px; margin: 0 auto; color: #FFF; text-transform: uppercase;">
      <tr>
        <th style="font-weight: 300; text-align: left;">Наименование</th>
        <th style="font-weight: 300; text-align: left; width:80px;">Цена</th>
        <th style="font-weight: 300; width:80px;">Кол-во</th>
        <th style="font-weight: 300; text-align: right; width:80px;">Сумма</th>
      </tr>
    </table>

    <table border="0" style="padding: 10px 0; border-bottom: 2px solid rgb(114, 211, 240); font-size: 14px; max-width: 100%; width: 600px; margin: 0 auto; color: #FFF; text-transform: uppercase;">
      ' . $table . '
    </table>


    <table border="0" style="padding: 20px 0 10px;  font-size: 14px; max-width: 100%; width: 600px; margin: 0 auto; color: #FFF; text-transform: uppercase;">
      <tr>
        <th style="text-align: left;">
          <a style="font-size: 20px; font-weight: 700; padding: 10px 25px; background-color: rgb(81, 254, 83); border-radius: 5px; color: #FFF; text-decoration: none;" href="tel:' . $phone . '">Позвонить</a>
        </th>

        <th style="font-weight: 300; text-align: right; width:180px;">
          Итого <span style="font-size: 24px; font-weight: 700;">' . $all_price . ' р.</span>
          '. $disconts . '
        </th>
      </tr>

    </table>



  </div>

  <table border="0" style="padding: 20px 0 10px;  font-size: 14px; max-width: 100%; width: 600px; margin: 0 auto; color: #999; text-transform: none;">
    <tr>
    <th style="text-align: left; font-weight: 300;">
      Телефон: ' . $partner_phone . '<br>
      E-Mail: ' . $email_partner . '
    </th>
    <th style="text-align: left; color: #00b8f6; font-size: 20px; text-transform: uppercase; font-weight: 700;">
      Спасибо за покупку!
    </th>

    </tr>

  </table>

  <div style="background-color: #00b8f6; text-align: center; padding: 15px;">
</div>

<a href="http://my.holiday-paint.ru/order" style="text-transform: uppercase; padding: 10px 20px; text-decoration: none;  margin: 45px auto; display: block; text-align: center;">
  <img alt="HoliCloude" src="http://my.holiday-paint.ru/btn.jpg" style="max-width: 300px; height: auto;"/>
</a>
</body>
</html>
';





      // Для отправки HTML-письма должен быть установлен заголовок Content-type
      // $headers = array();

      $headers  = 'MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=utf-8' . "\r\n";

      // // Дополнительные заголовки
      // $headers[] = 'To: Mary <mary@example.com>, Kelly <kelly@example.com>';
       $headers .= 'From: HOLIDAY PAINT <zakaz@holiday-paint.ru>';
      // $headers[] = 'Cc: birthdayarchive@example.com';
      // $headers[] = 'Bcc: birthdaycheck@example.com';

      // Отправляем
      mail($to, $subject, $message, $headers);
      mail($client, $subject_client, $message_client, $headers);


      $table_tree = 'users_info';
      $update_user = $mysqli->prepare("UPDATE $table_tree SET `last_sale`=NOW() WHERE `id`=?");
      $update_user->bind_param("i", $partner);
      $update_user->execute();


    } else $data['error'] = 3;

    $mysqli->close();
  }

} else $data['error'] = 1;

echo json_encode($data);

?>
