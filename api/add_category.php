<?php
if (!empty($_POST['data'])) {
  $category = json_decode( $_POST['data'] );
  $name = $category->{'name'};

  require_once 'config.php';
  $table = 'categories';

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    $mysqli->set_charset("utf8");
    $add_category = $mysqli->prepare("INSERT INTO $table (name) VALUES (?)");
    $add_category->bind_param("s", $name);
    $insert = $add_category->execute();
    $add_category->close();
    if ($insert) $data['error'] = 0;
    $mysqli->close();
  }
} else $data['error'] = 1;

echo json_encode($data);
?>
