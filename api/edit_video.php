<?php
if (!empty($_POST['data'])) {

  require_once 'config.php'; // подключаем скрипт
  $table = 'videos';
  $video = json_decode( $_POST['data'] );

  $name = $video->{'name'};
  $code = $video->{'code'};


  if (isset($video->{'id'})) {
    $act = 'edit';
    $id = $video->{'id'};
  } else {
    $act = 'new';
  }

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    //echo $items;
    $mysqli->set_charset("utf8");

    if ($act == 'new') {
      $edit_video = $mysqli->prepare("INSERT INTO $table (name, code) VALUES (?, ?)");
      $edit_video->bind_param("ss", $name, $code);
    }

    if ($act == 'edit') {
      $edit_news = $mysqli->prepare("UPDATE $table SET name=?, code=? WHERE id=?");
      $edit_news->bind_param("ssi", $name, $code, $id);
    }

    $edit = $edit_video->execute();
    if ($edit) $data['error'] = 0;
    if ($act == 'new') $data['id'] = $mysqli->insert_id;
    $edit_video->close();
    $mysqli->close();

  }

} else $data['error'] = 1;

echo json_encode($data);
?>
