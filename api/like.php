<?php
if (!empty($_POST['data'])) {
  $like = json_decode( $_POST['data'] );

  $type = $like->{'type'};
  $obj_id = $like->{'obj_id'};
  $user = $like->{'user'};
  $act = $like->{'act'};

  require_once 'config.php';
  $table = 'likes';
  $table_two = 'users_info';

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    $mysqli->set_charset("utf8");

    if ($type == 'idea') {

      if ($act == 'like') $action = 1;
      if ($act == 'dislike') $action = -1;
      $add_like = $mysqli->prepare("INSERT INTO $table (obj_id, type, user_id, action) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE action=?");
      $add_like->bind_param("isiii", $obj_id, $type, $user, $action, $action);
      $insert = $add_like->execute();
      if ($insert) $data['error'] = 0;

    } else {

      if ($act == 'like') {
        $add_like = $mysqli->prepare("INSERT IGNORE INTO $table (obj_id, type, user_id) VALUES (?, ?, ?)");
        $add_like->bind_param("isi", $obj_id, $type, $user);
        $point = 1;
      }

      if ($act == 'dislike') {
        $add_like = $mysqli->prepare("DELETE FROM $table WHERE obj_id = ? AND type = ? AND user_id = ?");
        $add_like->bind_param("isi", $obj_id, $type, $user);
        $point = -1;
      }


      if ($act == 'get') {
        $response_likes = $mysqli->prepare("SELECT * FROM $table WHERE obj_id = ? AND type = ?");
        $response_likes->bind_param("is", $obj_id, $type);
        $response_likes->execute();
        $response_likes->store_result();
        $data['response'] = $response_likes->num_rows;

        $add_like = $mysqli->prepare("SELECT * FROM $table WHERE obj_id = ? AND type = ? AND user_id = ?");
        $add_like->bind_param("isi", $obj_id, $type, $user);
      }

      $insert = $add_like->execute();
      if ($insert) $data['error'] = 0;


      if ($insert && $act !== 'get') {
        if ($type == 'partner') {
          $point = $point * 5;
          $edit_points = $mysqli->prepare("UPDATE $table_two SET points=points+? WHERE id=?");
          $edit_points->bind_param("ii", $point, $obj_id);
          $edit_points->execute();
          $edit_points->close();
        }
      }


      if ($act == 'get') {
        $result = $add_like->get_result();
        $row = $result->fetch_assoc();
        if (isset($row['id'])) {
          $data['like'] = 1;
        } else {
          $data['like'] = 0;
        }
      }

    }

    $add_like->close();

    $mysqli->close();
  }
} else $data['error'] = 1;

echo json_encode($data);
?>
