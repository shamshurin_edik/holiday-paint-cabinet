<?php
if (!empty($_POST['data'])) {
  $stock = json_decode( $_POST['data'] );
  $id = ($stock->{'id'});
  if (isset($stock->{'category'})) $category = $stock->{'category'};
  else $category = false;
  require_once 'config.php'; // подключаем конфиг

  $table = 'goods';
  $table_two = 'stock';
  $table_categories = 'categories';
  $data = array();

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {

    $mysqli->set_charset("utf8");
    if ($category) {
      $sell = $mysqli->prepare("SELECT $table.*, $table_two.quantity FROM $table INNER JOIN $table_two ON $table.id = $table_two.goods_id AND $table.category = ? AND $table_two.user_id = ?");
      $sell->bind_param("ii", $category, $id);
    } else {
      $sell = $mysqli->prepare("SELECT $table.*, $table_two.quantity FROM $table INNER JOIN $table_two ON $table.id = $table_two.goods_id AND $table_two.user_id = ?");
      $sell->bind_param("i", $id);

      $categories = $mysqli->prepare("SELECT * FROM $table_categories");
      $categories->execute();
      $result = $categories->get_result();
      while ($row = $result->fetch_assoc()) {
        $data['categories'][] = $row;
      }
      
    }
    $sell->execute();
    $result = $sell->get_result();
    while ($row = $result->fetch_assoc()) {
      $data['items'][] = $row;
    }
    $sell->close();
    $mysqli->close();
  }
} else $data['error'] = 1;
echo json_encode($data);

?>
