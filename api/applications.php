<?php
require_once 'config.php'; // подключаем скрипт

if (!empty($_POST['data'])) {
  $options = json_decode( $_POST['data'] );
  $user = ($options->{'user_id'});
  $table = 'applications';


  if (isset($options->{'location'})) {
    $location = $options->{'location'};

    $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
    if ($mysqli->connect_errno) {
      //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
      $data['error'] = 2;
    } else {
      $mysqli->set_charset("utf8");



      if ($location == 'all') {

        $get_applications = $mysqli->prepare("SELECT * FROM $table WHERE `user_id`=? ORDER BY `id` DESC");
        $get_applications->bind_param("i", $user);

        $get = $get_applications->execute();


        $result = $get_applications->get_result();
        $data = array();
        while ($row = $result->fetch_assoc()) {
          $data[] = $row;
        }

      } else {

        $get_applications = $mysqli->prepare("SELECT * FROM $table WHERE `user_id`=? AND `action`=? AND `status`=0 ORDER BY `id` DESC");
        $get_applications->bind_param("is", $user, $location);

        $get = $get_applications->execute();


        $result = $get_applications->get_result();
        $data = array();
        while ($row = $result->fetch_assoc()) {
          $data[] = $row;
        }

      }




      $get_applications->close();
      $mysqli->close();

    }
  }
}

  echo json_encode($data);
?>
