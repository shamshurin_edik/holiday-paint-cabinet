<?php
if (!empty($_POST['data'])) {

  require_once 'config.php'; // подключаем скрипт
  $table = 'lessons_progress';
  $lesson = json_decode( $_POST['data'] );

  $id = $lesson->{'id'};
  $user = $lesson->{'user'};
  $result = $lesson->{'result'};

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    //echo $items;
    $mysqli->set_charset("utf8");

    $edit_lesson = $mysqli->prepare("INSERT INTO $table (lesson, result, user) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE result=?");
    $edit_lesson->bind_param("iiii", $id, $result, $user, $result);



    $edit = $edit_lesson->execute();
    if ($edit) $data['error'] = 0;
    $edit_lesson->close();
    $mysqli->close();

  }

} else $data['error'] = 1;

echo json_encode($data);
?>
