<?php
require_once 'config.php'; // подключаем скрипт
$table = 'ideas_comments';
$table_two = 'users_info';
$config = json_decode( $_POST['data'] );
$id = $config->{'id'};
$data = [];

$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
if ($mysqli->connect_errno) {
  //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
  $data['error'] = 2;
} else {
  $mysqli->set_charset("utf8");

  $stmt = $mysqli->prepare("SELECT $table.*, $table_two.name, $table_two.photo FROM $table LEFT JOIN $table_two ON $table.author = $table_two.id WHERE $table.idea=?");
  $stmt->bind_param("i", $id);
  $stmt->execute();
  $result = $stmt->get_result();
  while ($row = $result->fetch_assoc()) {
    $data[] = $row;
  }
  $stmt->close();
  $mysqli->close();
  if (count($data) == 0) $data['error'] = 0;
}

echo json_encode($data);

?>
