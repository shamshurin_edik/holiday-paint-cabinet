<?php
$uploaddir = 'stencils/';
$extension = 'png';
$filename = uniqid() . '.' . $extension;
$uploadfile = $uploaddir . $filename;

if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
  $data['name'] = $filename;
  require_once 'config.php'; // подключаем скрипт
  $table = 'stencils';
  $link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD)
    or die('Не удалось соединиться: ' . mysqli_error());
  mysqli_select_db($link, DB_NAME) or die('Не удалось выбрать базу данных');
  mysqli_set_charset($link, 'utf8');

  $id = mysqli_real_escape_string($link, $_FILES['image']['name']);

  $query = "UPDATE $table SET `photo`='$filename' WHERE `id`='$id'";
  $result = mysqli_query($link, $query) or die('Запрос не удался: ' . mysqli_error($link));
  if ($result) $data['error'] = 0;
  else $data['error'] = 1;
  mysqli_close($link);

} else {
  $data['error'] = 1;
}

echo json_encode($data);
?>
