<?php
require_once 'config.php'; // подключаем скрипт

if (!empty($_POST['data'])) {
  $options = json_decode( $_POST['data'] );

  $user = $options->{'user_id'};



  $table = 'applications';

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    $mysqli->set_charset("utf8");


    $query = $mysqli->prepare("SELECT * FROM $table WHERE status=0 AND action=? AND user_id=?");
    $query->bind_param("si", $action, $user);

    $action = 'order';
    $query->execute();
    $query->store_result();
    $data['online'] = $query->num_rows;


    $action = 'stock';
    $query->execute();
    $query->store_result();
    $data['stock'] = $query->num_rows;

    $query->close();

    $table_two = 'lessons_progress';

    $query_two = $mysqli->prepare("SELECT * FROM $table_two WHERE result=0 AND user=?");
    $query_two->bind_param("i", $user);

    $query_two->execute();
    $query_two->store_result();
    $data['lessons'] = $query_two->num_rows;

    $query_two->close();

    $mysqli->close();

  }
} else $data['error'] = 1;

  echo json_encode($data);
?>
