<?php
if (!empty($_POST['data'])) {
  require_once 'config.php'; // подключаем скрипт
  $user = json_decode( $_POST['data'] );
  $id = ($user->{'id'});
  $name = ($user->{'name'});
  $city = ($user->{'city'});
  if (isset($user->{'phone'})) $phone = $user->{'phone'}; else $phone = 'null';
  if (isset($user->{'street'})) $street = $user->{'street'}; else $street = 'null';
  if (isset($user->{'house'})) $house = $user->{'house'}; else $house = 'null';
  if (isset($user->{'room'})) $room = $user->{'room'}; else $room = 'null';
  if (isset($user->{'phone'})) {
    $phone = $user->{'phone'};
    $phone = preg_replace("/[^0-9]/", '', $phone);
    if (strlen($phone) > 10) $phone = substr("$phone", -10);
  } else $phone = 'null';

  if (isset($user->{'story'})) $story = $user->{'story'}; else $story = 'null';
  if (isset($user->{'social'})) $social = json_encode($user->{'social'});

  $coordinates = ($user->{'coordinates'}[0] . ' ' . $user->{'coordinates'}[1]);
  $coordiantes_city = ($user->{'coordiantes_city'});
  $data = [];

  $table = 'users_info';
  $table_two = 'cities';

  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if ($mysqli->connect_errno) {
    //echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . //$mysqli->connect_error;
    $data['error'] = 2;
  } else {
    $mysqli->set_charset("utf8");
    $stmt = $mysqli->prepare("SELECT * FROM $table_two WHERE name=?");
    $stmt->bind_param("s", $city);
    $stmt->execute();
    $result = $stmt->get_result();
    $row = $result->fetch_assoc();
    if (isset($row['id'])) {
      $city_id = $row['id'];
    } else {
      $new_city = $mysqli->prepare("INSERT INTO $table_two(`name`,`longitude`,`latitude`) VALUES (?,?,?)");
      $new_city->bind_param("sdd", $city, $coordiantes_city[0],$coordiantes_city[1]);
      $new_city->execute();
      $city_id = $mysqli->insert_id;
    }

    $save_user = $mysqli->prepare("INSERT INTO $table (id, name, phone, city, street, house, room, coordinates, story, social) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE name=?, phone=?, city=?, street=?, house=?, room=?, coordinates=?, story=?, social=?");
    $save_user->bind_param("ississssssssissssss", $id, $name, $phone, $city_id, $street, $house, $room, $coordinates, $story, $social, $name, $phone, $city_id, $street, $house, $room, $coordinates, $story, $social);
    $save_user->execute();
    if ($save_user) {
      $data['error'] = 0;
    } else $data['error'] = 3;
  }


} else $data['error'] = 1;

echo json_encode($data);

?>
