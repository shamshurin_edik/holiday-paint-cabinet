if (process.env.NODE_ENV === 'production') {
  var api_url = 'https://my.holiday-paint.ru/api/';
} else {
  var api_url = 'http://my.holiday-paint.test/api/';
}

const config = {
  API_URL: api_url
}

export default config;
