import Vue from 'vue'
const Home = () => import(/* webpackChunkName: "home" */ './Home.vue');
const Signin = () => import(/* webpackChunkName: "home" */ './Signin.vue');
import "../node_modules/bootstrap/scss/bootstrap-grid.scss";
import "./style/main.scss";

import VueRouter from 'vue-router'
Vue.use(VueRouter)

const routes = [
  { path: '/', component: Signin },
  { path: '/:open', component: Home }
]

const router = new VueRouter({
  mode: 'history',
  routes // сокращение от `routes: routes`
})

const app = new Vue({
  el: '#app',
  data: {
    auth: false,
    user: {
      id: null,
      email: '',
      key: ''
    }
  },
  router,
});
